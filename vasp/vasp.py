from pathlib import Path
import shutil

from ase import Atoms
from ase.calculators.vasp import Vasp

class VaspCalculation:
    """
    Class to run vasp calculations. Should eventually use a template and a
    profile. For now just uses the old vasp calculator.
    
    Can copy output files to a new directory for follow-up calculations
    and run vasp in a work directory.
    """
    def __init__(self, atoms, parameters, directory):
        self.atoms = atoms
        self.parameters = parameters
        self.directory = directory
        
    def copy_car_files(self, directory):
        # What will happen if the files are not there?
        files = ['WAVECAR', 'CHGCAR', 'CHG', 'CONTCAR']
        for file in files:
            shutil.copyfile(self.directory / file, directory / file)
            
    def run(self):
        calc = Vasp(atoms=self.atoms, directory=self.directory, **self.parameters)
        self.atoms.calc = calc
        self.atoms.get_potential_energy()
        

def groundstate(atoms: Atoms,
                parameters: dict,
                directory: Path) -> VaspCalculation:
    """
    Runs a groundstate calculation with VASP using the VaspCalculation class

    Parameters
    ----------
    atoms : ase.atoms.Atoms
        An ASE atoms object
    parameters : dict
        input parameters for a vasp calculation. Includes not only INCAR
        parameters, but also kpts, etc.
    directory : pathlib.PosixPath
        Path to the working directory.

    Returns
    -------
    calc : VaspCalculation
        The vasp calculation containing the atoms object with the results.

    """
    directory.mkdir(exist_ok=True, parents=True)
    
    # Force an actual ground state calculation, i.e. set NSW=0, etc.
    # parameters['nsw'] = 0 

    calc = VaspCalculation(atoms, parameters, directory)
    calc.run()
    return calc
        
def bandstructure(gs: VaspCalculation,
                  bandpath: str,
                  parameters: dict,
                  directory: Path,
                  total_kpoints: int=100) -> VaspCalculation:
    """
    

    Parameters
    ----------
    gs : VaspCalculation
        Groundstate calculation
    bandpath : str
        String containing the high symmetry points of the kpath. E.g. 'LGXU'
        WARNING! FOR NOW DOES NOT WORK FOR NON-CONTINOUS PATHS e.g. LGXU,KG!!
    parameters : dict
        Calculation parameters
    directory : Path
        Work directory.
    total_kpoints : int, optional
        Total number of kpoints for all the high-symmetry directions.
        The default is 100.

    Returns
    -------
    VaspCalculation
        Bandstrucuture calculation including the atoms object containing the
        results.

    """
    atoms = gs.atoms.copy()

    directory.mkdir(exist_ok=True, parents=True)
    
    gs.copy_car_files(directory)
    
    bsparameters = {
        **gs.parameters,
        #kpts': bandpath.kpts,
        'kpts': {'path': bandpath, 'npoints': total_kpoints},
        # Forcing ICHARG=11, and ismear=0. Anything else (EDIFF, SIGMA,...)?
        'icharg': 11,
        'ismear': 0,
        **parameters}
    
    calc = VaspCalculation(atoms, bsparameters, directory)
    calc.run()
    return calc
    



def main():
    from ase.build import bulk
    directory = Path('work/vasp')
    directory.mkdir(exist_ok=True, parents=True)
    
    atoms = bulk('Si', crystalstructure='fcc', a=3.9)

    
    gs = groundstate(atoms, {'kpts': [15, 15, 15],
                             'nsw': 0,
                             'xc': 'PBE',
                             'encut': 240,
                             'ismear': -5},
                     directory)
    print(gs.atoms.get_potential_energy())

    bandstructure_directory = directory / 'bandstructure'
    bandpath = 'LGXU'
    bscalc = bandstructure(gs, bandpath,
                           {'xc': 'PBE', 'encut': 240},
                           bandstructure_directory, 80)
    bs = bscalc.atoms.calc.band_structure()
    efermi = bscalc.atoms.calc.get_fermi_level()
    bs.plot(emin=-15+efermi, emax=15+efermi, filename='Si_bandstructure.png')
    
if __name__ == '__main__':
    main()
