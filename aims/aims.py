import os
import shutil

import numpy as np

from glob import glob
from pathlib import Path

from ase.build import bulk
from ase.calculators.aims import Aims
from ase.io.aims import read_aims_output


class AimsCalculation:
    def __init__(self, atoms, parameters, directory):
        self.atoms = atoms
        self.parameters = parameters
        self.directory = Path(directory)

    def copy_restart_directory(self, directory, overwrite=False):
        directory.mkdir(parents=True, exist_ok=overwrite)
        restart_filenames = f"{self.directory}/*.csc"
        for file in glob(restart_filenames):
            shutil.copy(file, directory / file.split("/")[-1])

    def read_results(self):
        template = AimsTemplate()
        return template.read_results(self.directory)


def check_directory(directory, overwrite):
    directory = Path(directory)
    directory.mkdir(exist_ok=True, parents=True)
    if not overwrite and (directory / "aims.out").exists():
        raise IOError("Calculation already exists in the directory.")

    return directory


def groundstate(atoms, parameters, directory, properties=["energy"], overwrite=False):
    directory = check_directory(directory, overwrite)

    if "elsi_restart" not in parameters:
        parameters["elsi_restart"] = "read_and_write 1000"

    atoms.calc = Aims(directory=directory, **parameters)

    atoms.calc.calculate(atoms, properties, system_changes=None)
    return AimsCalculation(atoms, parameters, directory)


def gw(atoms, parameters, directory, overwrite=False):
    """A GW calculation for aims

    Args:
        atoms (ase.Atoms): a relaxed Atoms object to calculate GW for
        parameters (dict): general parameters for the calculation
        directory (:obj:`list` or :obj:`pathlib.Path`): a directory to put the calculation into
        overwrite (boolean): a directory overwrite flag

    Returns:
        an AimsCalculation object
    """

    directory = check_directory(directory, overwrite)

    # checks
    if sum(atoms.pbc) not in (0, 3):
        raise ValueError(
            "GW calculation can be perform only on bulk or cluster geometry"
        )

    if all(atoms.pbc):
        if "qpe_calc" not in parameters:
            parameters["qpe_calc"] = "gw_expt"
        elif parameters["qpe_calc"] != "gw_expt":
            raise ValueError("`qpe_calc` has to be `gw_expt` for periodic calculations")
    else:
        if "qpe_calc" not in parameters:
            parameters["qpe_calc"] = "gw"
        elif ("sc_self_energy" not in parameters) or (
            parameters["qpe_calc"] not in ("gw", "ev_scgw", "ev_scgw0", "mp2")
        ):
            raise ValueError("`qpe_calc` is not correct for cluster calculation")

    if ("anacon_type" not in parameters) or (
        str(parameters["anacon_type"]) not in ("two-pole", "pade", "0", "1")
    ):
        raise KeyError(
            "GW calc should have `anacon_type` parameter with the value from [`two-pole`, `pade`]"
        )

    atoms.calc = Aims(directory=directory, **parameters)
    atoms.calc.calculate(atoms, ["energy"], None)
    return AimsCalculation(atoms, parameters, directory)
